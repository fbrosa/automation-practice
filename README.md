# Automation Practice

A script for testing the ordering of a product at http://www.automationpractice.com using Selenium, Behave and Python.

## Installation

1. Clone the repository to your local machine and switch to the project directory.
```bash
git clone https://gitlab.com/fbrosa/automation-practice.git
cd automation-practice
```

2. Install all the required packages.
```bash
pip install -r requirements.txt
```

3. Install Firefox.

## Usage

Run behave at the top-level directory of the project to run the tests.
```bash
behave
```

## Test Data

The customer data for creating an account at the website is kept at `data/customer.json`. Note that a new email must be given for each test run.
