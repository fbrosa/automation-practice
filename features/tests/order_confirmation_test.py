class Order_Confirmation_Test:
    def has_order_been_completed_successfully(self, order_confirmation_page):
        return order_confirmation_page.alert == "Your order on My Store is complete."
