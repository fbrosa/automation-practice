class Shopping_Cart_Summary_Test:
    def has_product_been_correctly_added_to_cart(self, shopping_cart_summary_page, product):
        return shopping_cart_summary_page.product_name == product \
            and shopping_cart_summary_page.unit_price * shopping_cart_summary_page.qty == shopping_cart_summary_page.total_product_price \
            and shopping_cart_summary_page.total_product_price == shopping_cart_summary_page.total_products \
            and shopping_cart_summary_page.total_products + shopping_cart_summary_page.total_shipping == shopping_cart_summary_page.total_price_without_tax \
            and shopping_cart_summary_page.total_price_without_tax + shopping_cart_summary_page.tax == shopping_cart_summary_page.total_price \
            and shopping_cart_summary_page.summary_products_quantity == shopping_cart_summary_page.qty
