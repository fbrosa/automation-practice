class Addresses_Test:
    def is_address_correct(self, addresses_page, customer):
        return addresses_page.first_name_last_name == customer["first_name"] + " " + customer["last_name"] \
            and addresses_page.company == customer["company"] \
            and addresses_page.address1_address2 == customer["address"] + " " + customer["address_line_2"] \
            and addresses_page.country_name == customer["country"] \
            and addresses_page.phone == customer["home_phone"] \
            and addresses_page.phone_mobile == customer["mobile_phone"]
