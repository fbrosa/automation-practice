class Payment_Test:
    def is_total_price_correct(self, payment_page): 
        return payment_page.unit_price * payment_page.qty == payment_page.total_product_price \
            and payment_page.total_product_price == payment_page.total_products \
            and payment_page.total_products + payment_page.total_shipping == payment_page.total_price
