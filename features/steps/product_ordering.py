from behave import *
import json

from util.util import CUSTOMER_FILE
from util.util import access_website

from features.pages.home_page import Home_Page
from features.pages.search_page import Search_Page
from features.pages.product_page import Product_Page
from features.pages.cart_overlay_page import Cart_Overlay_Page
from features.pages.shopping_cart_summary_page import Shopping_Cart_Summary_Page
from features.pages.authentication_page import Authentication_Page
from features.pages.create_an_account_page import Create_An_Account_Page
from features.pages.addresses_page import Addresses_Page
from features.pages.shipping_page import Shipping_Page
from features.pages.payment_page import Payment_Page
from features.pages.order_summary_page import Order_Summary_Page
from features.pages.order_confirmation_page import Order_Confirmation_Page

from features.tests.shopping_cart_summary_test import Shopping_Cart_Summary_Test
from features.tests.payment_test import Payment_Test
from features.tests.addresses_test import Addresses_Test
from features.tests.order_confirmation_test import Order_Confirmation_Test


@given("the customer goes to the store")
def step_impl(context):
    access_website(context, "http://automationpractice.com/")


@when("the customer chooses the {product}")
def step_impl(context, product):
    home_page = Home_Page(context)
    home_page.search(product)

    search_page = Search_Page(context)
    search_page.choose(product)


@when("the customer adds the {product} to the cart")
def step_impl(context, product):
    product_page = Product_Page(context)
    product_page.add_to_cart()


@when("the customer proceeds to checkout")
def step_impl(context):
    cart_overlay_page = Cart_Overlay_Page(context)
    cart_overlay_page.proceed_to_checkout()
    

@then("the customer should have the {product} added to the cart")
def step_impl(context, product):
    shopping_cart_summary_test = Shopping_Cart_Summary_Test()
    shopping_cart_summary_page = Shopping_Cart_Summary_Page(context)
    
    assert shopping_cart_summary_test \
        .has_product_been_correctly_added_to_cart(shopping_cart_summary_page, product)
    

@when("the customer proceeds to authentication")
def step_impl(context):
    shopping_cart_summary_page = Shopping_Cart_Summary_Page(context)
    shopping_cart_summary_page.proceed_to_checkout()


@when("the customer creates an account")
def step_impl(context):

    with open(CUSTOMER_FILE) as cf:
        customer = json.load(cf)

    authentication_page = Authentication_Page(context)
    authentication_page.create_an_account(customer["email"])

    create_an_account_page = Create_An_Account_Page(context)
    create_an_account_page.fill_out(customer)
    create_an_account_page.register()
        

@then("the customer should see their address")
def step_impl(context):
    addresses_test = Addresses_Test()
    with open(CUSTOMER_FILE) as cf:
        customer = json.load(cf)
    
    addresses_page = Addresses_Page(context)

    assert addresses_test.is_address_correct(addresses_page, customer)
   

@when("the customer proceeds to shipping")
def step_impl(context):
    addresses_page = Addresses_Page(context)
    addresses_page.proceed_to_checkout()
    

@when("the customer agrees to the terms of service")
def step_impl(context):
    shipping_page = Shipping_Page(context)
    shipping_page.agree_to_terms_of_service()
    shipping_page.proceed_to_checkout()
    

@then("the customer should see the total price")
def step_impl(context):
    payment_test = Payment_Test()
    payment_page = Payment_Page(context)

    assert payment_test.is_total_price_correct(payment_page)


@when("the customer chooses to pay by bank wire")
def step_impl(context):
    payment_page = Payment_Page(context)
    payment_page.pay_by_bank_wire()


@when("the customer chooses to pay by check")
def step_impl(context):
    payment_page = Payment_Page(context)
    payment_page.pay_by_check()


@when("the customer confirms the order")
def step_impl(context):
    order_summary_page = Order_Summary_Page(context)
    order_summary_page.confirm_order()


@then("the customer should have completed the order")
def step_impl(context):
    order_confirmation_test = Order_Confirmation_Test()
    order_confirmation_page = Order_Confirmation_Page(context)

    assert order_confirmation_test.has_order_been_completed_successfully(order_confirmation_page)
