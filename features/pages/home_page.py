class Home_Page:
    def __init__(self, context):
        self.search_field = context.browser.find_element_by_css_selector("#search_query_top")
        self.search_button = context.browser.find_element_by_css_selector("button[name='submit_search']")

    def search(self, product):
        self.search_field.send_keys(product)
        self.search_button.click()
        
