from ..util.util import find_element_by_css_selector_once_visible

class Cart_Overlay_Page:
    def __init__(self, context):
        self.proceed_to_checkout_button = \
            find_element_by_css_selector_once_visible("a[title='Proceed to checkout']", context.browser)

    def proceed_to_checkout(self):
        self.proceed_to_checkout_button.click()
        
