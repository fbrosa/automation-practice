class Shipping_Page:
    def __init__(self, context):
        self.terms_of_service_checkbox = context.browser.find_element_by_css_selector("#cgv")
        self.proceed_to_checkout_button = context.browser.find_element_by_css_selector("button[name=processCarrier]")

    def agree_to_terms_of_service(self):
        self.terms_of_service_checkbox.click()

    def proceed_to_checkout(self):
        self.proceed_to_checkout_button.click()
