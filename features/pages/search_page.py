class Search_Page:

    def __init__(self, context):
        self.product_links = context.browser.find_elements_by_css_selector(".right-block a[class='product-name']")
    
    def choose(self, product):
        chosen_product_link = next((pl for pl in self.product_links if pl.text == product))
        chosen_product_link.click()
