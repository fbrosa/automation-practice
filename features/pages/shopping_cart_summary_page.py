import re
from ..util.util import convert_price_to_decimal

class Shopping_Cart_Summary_Page:
    def __init__(self, context):
        self.product_name = \
            context.browser.find_element_by_css_selector("td[class='cart_description'] p[class='product-name'] a").text

        self.unit_price = \
            convert_price_to_decimal(context.browser.find_element_by_css_selector("td[class='cart_unit'] span[class='price'] span[class='price']").text)

        self.qty = \
            int(context.browser.find_element_by_css_selector("td[class='cart_quantity text-center'] input[class='cart_quantity_input form-control grey']").get_attribute("value"))

        self.total_product_price = \
            convert_price_to_decimal(context.browser.find_element_by_css_selector("td[class='cart_total'] span[class='price']").text)


        self.total_products = \
            convert_price_to_decimal(context.browser.find_element_by_css_selector("#total_product").text)

        self.total_shipping = \
            convert_price_to_decimal(context.browser.find_element_by_css_selector("#total_shipping").text)

        self.total_price_without_tax = \
            convert_price_to_decimal(context.browser.find_element_by_css_selector("#total_price_without_tax").text)

        self.tax = \
            convert_price_to_decimal(context.browser.find_element_by_css_selector("#total_tax").text)

        self.total_price = \
            convert_price_to_decimal(context.browser.find_element_by_css_selector("#total_price").text)

        self.summary_products_quantity = \
            int(re.search(r"\d+", context.browser.find_element_by_css_selector("#summary_products_quantity").text).group())
            
        self.proceed_to_checkout_button = \
            context.browser.find_element_by_css_selector("p[class='cart_navigation clearfix'] a[title='Proceed to checkout']")

    def proceed_to_checkout(self):
        self.proceed_to_checkout_button.click()
