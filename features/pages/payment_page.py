from ..util.util import convert_price_to_decimal

class Payment_Page:
    def __init__(self, context):
        self.unit_price = \
            convert_price_to_decimal(context.browser.find_element_by_css_selector("#cart_summary td[class='cart_unit'] span[class='price'] span[class='price']").text)

        self.qty = \
            int(context.browser.find_element_by_css_selector("#cart_summary  td[class='cart_quantity text-center'] span").text)

        self.total_product_price = \
            convert_price_to_decimal(context.browser.find_element_by_css_selector("#cart_summary  td[class='cart_total'] span[class='price']").text)

        self.total_products = \
            convert_price_to_decimal(context.browser.find_element_by_css_selector("#total_product").text)

        self.total_shipping = \
            convert_price_to_decimal(context.browser.find_element_by_css_selector("#total_shipping").text)

        self.total_price = \
            convert_price_to_decimal(context.browser.find_element_by_css_selector("#total_price").text)


        self.pay_by_bank_wire_button = context.browser.find_element_by_css_selector("a[class='bankwire']")
        self.pay_by_check_button = context.browser.find_element_by_css_selector("a[class='cheque']")

    def pay_by_bank_wire(self):
        self.pay_by_bank_wire_button.click()

    def pay_by_check(self):
        self.pay_by_check_button.click()


