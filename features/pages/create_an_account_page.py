from selenium.webdriver.support.ui import Select

from ..util.util import find_element_by_css_selector_once_visible

class Create_An_Account_Page:
    def __init__(self, context):
        self.mr_radio = find_element_by_css_selector_once_visible("#id_gender1", context.browser)
        self.mrs_radio = context.browser.find_element_by_css_selector("#id_gender2")
        self.first_name_field = context.browser.find_element_by_css_selector("#customer_firstname")
        self.last_name_field = context.browser.find_element_by_css_selector("#customer_lastname")
        self.password_field = context.browser.find_element_by_css_selector("#passwd")
        self.day_of_birth_select = Select(context.browser.find_element_by_css_selector("#days"))
        self.month_of_birth_select = Select(context.browser.find_element_by_css_selector("#months"))
        self.year_of_birth_select = Select(context.browser.find_element_by_css_selector("#years"))
        self.newsletter_checkbox = context.browser.find_element_by_css_selector("#newsletter")
        self.newsletter_checkbox = context.browser.find_element_by_css_selector("#newsletter")
        self.opt_in_checkbox = context.browser.find_element_by_css_selector("#optin")
        self.company_field = context.browser.find_element_by_css_selector("#company")
        self.address_field = context.browser.find_element_by_css_selector("#address1")
        self.address_line_2_field = context.browser.find_element_by_css_selector("#address2")
        self.city_field = context.browser.find_element_by_css_selector("#city")
        self.state_select = Select(context.browser.find_element_by_css_selector("#id_state"))
        self.postal_code_field = context.browser.find_element_by_css_selector("#postcode")
        self.country_select = Select(context.browser.find_element_by_css_selector("#id_country"))
        self.additional_information_box = context.browser.find_element_by_css_selector("#other")
        self.home_phone_field = context.browser.find_element_by_css_selector("#phone")
        self.mobile_phone_field = context.browser.find_element_by_css_selector("#phone_mobile")
        self.address_alias_field = context.browser.find_element_by_css_selector("#alias")
        self.register_button = context.browser.find_element_by_css_selector("#submitAccount")

    def fill_out(self, customer):
        if customer['title'] == "Mr.":
            self.mr_radio.click()
        elif customer['title'] == "Mrs.":
            self.mrs_radio.click()

        self.first_name_field.send_keys(customer['first_name'])
        self.last_name_field.send_keys(customer['last_name'])
        self.password_field.send_keys(customer['password'])
        self.day_of_birth_select.select_by_visible_text(customer["day_of_birth"])
        self.month_of_birth_select.select_by_visible_text(customer["month_of_birth"])        
        self.year_of_birth_select.select_by_visible_text(customer["year_of_birth"])

        if customer["has_signed_up_for_newsletter"] == True:
            self.newsletter_checkbox.click()

        if customer["has_opted_in"] == True:
            self.opt_in_checkbox.click()    

        self.company_field.send_keys(customer["company"])
        self.address_field.send_keys(customer["address"])
        self.address_line_2_field.send_keys(customer["address_line_2"])
        self.city_field.send_keys(customer["city"])
        self.state_select.select_by_visible_text(customer["state"])
        self.postal_code_field.send_keys(customer["postal_code"])
        self.country_select.select_by_visible_text(customer["country"])
        self.additional_information_box.send_keys(customer["additional_information"])
        self.home_phone_field.send_keys(customer["home_phone"])
        self.mobile_phone_field.send_keys(customer["mobile_phone"])
        self.address_alias_field.send_keys(customer["address_alias"])

    def register(self):
        self.register_button.click()

                                       
                                       
                                       
                                       
                                       
                                       
                                       
                                       
                                       
                                       
