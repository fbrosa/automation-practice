class Addresses_Page:
    def __init__(self, context):
        self.first_name_last_name = context.browser.find_element_by_css_selector("#address_delivery li[class='address_firstname address_lastname']").text
        self.company = context.browser.find_element_by_css_selector("#address_delivery li[class='address_company']").text
        self.address1_address2 = context.browser.find_element_by_css_selector("#address_delivery li[class='address_address1 address_address2']").text
        self.country_name = context.browser.find_element_by_css_selector("#address_delivery li[class='address_country_name']").text
        self.phone = context.browser.find_element_by_css_selector("#address_delivery li[class='address_phone']").text
        self.phone_mobile = context.browser.find_element_by_css_selector("#address_delivery li[class='address_phone_mobile']").text

        self.proceed_to_checkout_button = \
            context.browser.find_element_by_css_selector("button[name='processAddress']")

    def proceed_to_checkout(self):
        self.proceed_to_checkout_button.click() 
        
