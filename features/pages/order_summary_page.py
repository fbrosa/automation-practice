class Order_Summary_Page:
    def __init__(self, context):
        self.i_confirm_my_order_button = context.browser.find_element_by_css_selector("button[class='button btn btn-default button-medium']")

    def confirm_order(self):
        self.i_confirm_my_order_button.click()
