class Authentication_Page:
    def __init__(self, context):
        self.email_address_field = context.browser.find_element_by_css_selector("#email_create")
        self.create_an_account_button = context.browser.find_element_by_css_selector("#SubmitCreate")

    def create_an_account(self, email):
        self.email_address_field.send_keys(email)
        self.create_an_account_button.click()
