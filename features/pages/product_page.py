class Product_Page:
    def __init__(self, context):
        self.add_to_cart_button = context.browser.find_element_by_css_selector("button[name='Submit']")

    def add_to_cart(self):
        self.add_to_cart_button.click()
