Feature: Product ordering
    As a customer
    I want to order a product
    So that I have the product sent to my address after paying for it

    Scenario Outline: Ordering a product
        Given the customer goes to the store
        When the customer chooses the <product>
        And the customer adds the <product> to the cart
        And the customer proceeds to checkout
        Then the customer should have the <product> added to the cart
        When the customer proceeds to authentication
        And the customer creates an account
        Then the customer should see their address
        When the customer proceeds to shipping
        And the customer agrees to the terms of service
        Then the customer should see the total price
        When the customer chooses to pay by <payment_method>
        And the customer confirms the order
        Then the customer should have completed the order

    Examples:
        | product                     | payment_method |
        | Faded Short Sleeve T-shirts | check          |
