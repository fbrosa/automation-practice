from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException, StaleElementReferenceException

from decimal import Decimal

CUSTOMER_FILE = "data/customer.json"

def access_website(context, url):
    context.browser.get(url)


def find_element_by_css_selector_once_visible(css_selector, browser):
    TIMEOUT = 5
    try:
        wait = WebDriverWait(browser, TIMEOUT)
        return wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR, css_selector))) 
    except(TimeoutException, StaleElementReferenceException):
        print("Element located at {} did not become visible".format(css_selector)) 


def convert_price_to_decimal(price):
    return Decimal(price.strip("$").replace(",", ""))
    
    
